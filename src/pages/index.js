import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>"Ceci est le site web de Gabriel !!!"</h1>
    <Link to="/page-2/">aller à la page 2</Link>
  </Layout>
)

export default IndexPage
